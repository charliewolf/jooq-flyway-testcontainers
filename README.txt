
This is a set of classes that allow you to tie jooq, gradle, testcontainers, and flyway together
such that jooq code generation takes place in testcontainers with automatic flyway migrations.

Special care was taken to ensure this works with both legacy and current versions of Flyway.
I have tested this from Flyway 5.4 up to 9.8.

I have only tested it with postgres but theoretically it should work with:
 * PostgreSQL
 * MariaDB
 * MySQL
 * YugabyteDB
 * CockroachDB
 * MS-SQL
 * Oracle

Some config settings you may adapt are below.

===

plugins {
    id 'nu.studer.jooq' version '7.1.1'
}

repositories {
    mavenCentral()
    maven {
      url 'https://gitlab.com/api/v4/projects/41224216/packages/maven'
    }
}

jooq {
    configurations {
       main {
            generateSchemaSourceOnCompilation = true
            generationTool {
                generator {
                    name = 'org.jooq.codegen.DefaultGenerator'
                    database {
                        name = 'is.wolf.util.FlywayPostgresMigrationDatabase'
                        inputSchema = 'public'
                        properties {
                            property { 
                              key = 'dockerImage'
                              value = 'postgres:13-alpine'
                            }
                        }
                    }
                    generate {
                        jpaAnnotations = true
                    }
                }
            }
        }
    }
}

dependencies {
    jooqGenerator group: 'org.flywaydb', name: 'flyway-core', version: '9.8.1'
    jooqGenerator group: 'org.slf4j', name: 'slf4j-simple', version: '1.7.36'
    jooqGenerator 'is.wolf:jooq-flyway-testcontainers:1.0.4'
    jooqGenerator group: 'org.testcontainers', name: 'postgresql', version: '1.17.6'
    jooqGenerator group: 'org.postgresql', name: 'postgresql', version: '42.5.0'

}

