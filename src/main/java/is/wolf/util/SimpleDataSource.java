package is.wolf.util;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.SQLException;
import java.util.Objects;
import java.util.Properties;
import java.util.logging.Logger;

public class SimpleDataSource implements DataSource {
    private final Driver driver;
    private final String uri;
    private final String username;
    private final String password;

    public SimpleDataSource(String driverClassName, String uri, String username, String password){
        try {
            driver = (Driver)(Class.forName(driverClassName).getDeclaredConstructor().newInstance());
        } catch(ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException ex){
            throw new RuntimeException(ex);
        } catch(InvocationTargetException ex){
            if(ex.getTargetException() instanceof RuntimeException){
                throw (RuntimeException)ex.getTargetException();
            } else {
                throw new RuntimeException(ex);
            }
        }
        this.uri = uri;
        this.username = username;
        this.password = password;
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public void setLoginTimeout(int timeout) throws SQLException {
        throw new UnsupportedOperationException("setLoginTimeout");
    }

    @Override
    public PrintWriter getLogWriter() {
        throw new UnsupportedOperationException("getLogWriter");
    }

    @Override
    public void setLogWriter(PrintWriter pw) throws SQLException {
        throw new UnsupportedOperationException("setLogWriter");
    }


    @Override
    public Connection getConnection() throws SQLException {
        return this.getConnection(username, password);
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        Properties properties = new Properties();
        if(Objects.nonNull(username)){
            properties.setProperty("user", username);
        }
        if (Objects.nonNull(password)) {
            properties.setProperty("password", password);
        }
        return driver.connect(uri, properties);
    }

    @Override
    public Logger getParentLogger() {
        return Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T unwrap(Class<T> iface) throws SQLException {
        if (iface.isInstance(this)) {
            return (T) this;
        }
        throw new SQLException("DataSource of type [" + getClass().getName() +
                "] cannot be unwrapped as [" + iface.getName() + "]");
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return iface.isInstance(this);
    }

}
