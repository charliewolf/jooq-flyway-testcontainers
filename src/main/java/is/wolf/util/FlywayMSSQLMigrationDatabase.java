package is.wolf.util;

import org.jooq.DSLContext;
import org.jooq.meta.sqlserver.SQLServerDatabase;
import org.testcontainers.containers.MSSQLServerContainer;

public class FlywayMSSQLMigrationDatabase extends SQLServerDatabase {


    private TestContainersConnectionManager testContainersConnectionManager = new TestContainersConnectionManager(MSSQLServerContainer.class);

    @Override
    protected DSLContext create0() {
        if(getConnection() == null) {
            setConnection(testContainersConnectionManager.getInternalConnection(getProperties()));
        }
        return super.create0();
    }

    @Override
    public void close() {
        testContainersConnectionManager.closeConnection();
        super.close();
    }

}