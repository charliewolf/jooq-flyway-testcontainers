package is.wolf.util;

import org.jooq.DSLContext;
import org.jooq.meta.mysql.MySQLDatabase;
import org.testcontainers.containers.MySQLContainer;


public class FlywayMysqlMigrationDatabase extends MySQLDatabase {


    private TestContainersConnectionManager testContainersConnectionManager = new TestContainersConnectionManager(MySQLContainer.class);

    @Override
    protected DSLContext create0() {
        if(getConnection() == null) {
            setConnection(testContainersConnectionManager.getInternalConnection(getProperties()));
        }
        return super.create0();
    }

    @Override
    public void close() {
        testContainersConnectionManager.closeConnection();
        super.close();
    }

}