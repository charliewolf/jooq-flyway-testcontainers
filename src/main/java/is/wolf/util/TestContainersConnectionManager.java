package is.wolf.util;

import org.flywaydb.core.Flyway;
import org.jooq.tools.StringUtils;
import org.jooq.tools.jdbc.JDBCUtils;
import org.testcontainers.containers.JdbcDatabaseContainer;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class TestContainersConnectionManager {
    private Connection connection;
    private final Class<? extends JdbcDatabaseContainer> clazz;

    TestContainersConnectionManager(Class<? extends JdbcDatabaseContainer> clazz){
        this.clazz = clazz;
    }

    protected Connection getInternalConnection(Properties properties
    ) {
        if (connection == null) {
            try {
                createInternalConnection(properties);
            } catch (final Exception e) {
                throw new RuntimeException("Failed to launch DB container and run migration", e);
            }
        }
        return connection;
    }

    private void createInternalConnection(Properties properties) throws SQLException {
        String dockerImage = properties.getProperty("dockerImage");
        var placeholders = properties.stringPropertyNames().stream().filter(p -> p.startsWith("flywayPlaceholder_")).map(p -> {
            return new AbstractMap.SimpleEntry<String, String>(p.substring(18), properties.getProperty(p));
        }).map(e -> (Map.Entry<String, String>)e).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        JdbcDatabaseContainer<?> container;
        try {
            if (StringUtils.isBlank(dockerImage)) {
                container = clazz.getDeclaredConstructor().newInstance();
            } else {
                container = clazz.getDeclaredConstructor(String.class).newInstance(dockerImage);
            }

            container.start();

            is.wolf.util.SimpleDataSource dataSource = new is.wolf.util.SimpleDataSource(container.getDriverClassName(), container.getJdbcUrl(), container.getUsername(), container.getPassword());
            var flywayConfiguration = Flyway.configure().placeholders(placeholders).dataSource(dataSource);
            Optional.ofNullable(properties.getProperty("flywaySchemas")).map(s -> s.split(",")).ifPresent(flywayConfiguration::schemas);
            Optional.ofNullable(properties.getProperty("flywayLocations")).map(s -> s.split(",")).ifPresent(flywayConfiguration::locations);
            final Flyway flyway = flywayConfiguration.load();
            flyway.getClass().getDeclaredMethod("migrate").invoke(flyway);

            connection = dataSource.getConnection();
        } catch(NoSuchMethodException | IllegalAccessException | InstantiationException ex){
            throw new RuntimeException(ex);
        } catch(InvocationTargetException ex){
            if(ex.getTargetException() instanceof RuntimeException){
                throw (RuntimeException)ex.getTargetException();
            } else {
                throw new RuntimeException(ex);
            }
        }
    }

    public void closeConnection(){
        JDBCUtils.safeClose(connection);
        connection = null;
    }
}
