package is.wolf.util;


import org.jooq.DSLContext;
import org.jooq.meta.cockroachdb.CockroachDBDatabase;
import org.testcontainers.containers.CockroachContainer;

public class FlywayCockroachDBMigrationDatabase extends CockroachDBDatabase {


    private TestContainersConnectionManager testContainersConnectionManager = new TestContainersConnectionManager(CockroachContainer.class);

    @Override
    protected DSLContext create0() {
        if(getConnection() == null) {
            setConnection(testContainersConnectionManager.getInternalConnection(getProperties()));
        }
        return super.create0();
    }

    @Override
    public void close() {
        testContainersConnectionManager.closeConnection();
        super.close();
    }

}