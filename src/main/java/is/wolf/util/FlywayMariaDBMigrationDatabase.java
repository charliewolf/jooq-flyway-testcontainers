package is.wolf.util;

import org.jooq.DSLContext;
import org.jooq.meta.mariadb.MariaDBDatabase;
import org.testcontainers.containers.MariaDBContainer;


public class FlywayMariaDBMigrationDatabase extends MariaDBDatabase {

    private TestContainersConnectionManager testContainersConnectionManager = new TestContainersConnectionManager(MariaDBContainer.class);

    @Override
    protected DSLContext create0() {
        if(getConnection() == null) {
            setConnection(testContainersConnectionManager.getInternalConnection(getProperties()));
        }
        return super.create0();
    }

    @Override
    public void close() {
        testContainersConnectionManager.closeConnection();
        super.close();
    }

}