package is.wolf.util;

import org.jooq.DSLContext;
import org.jooq.meta.postgres.PostgresDatabase;
import org.testcontainers.containers.PostgreSQLContainer;


public class FlywayPostgresMigrationDatabase extends PostgresDatabase {


    private TestContainersConnectionManager testContainersConnectionManager = new TestContainersConnectionManager(PostgreSQLContainer.class);

    @Override
    protected DSLContext create0() {
        if(getConnection() == null) {
            setConnection(testContainersConnectionManager.getInternalConnection(getProperties()));
        }
        return super.create0();
    }

    @Override
    public void close() {
        testContainersConnectionManager.closeConnection();
        super.close();
    }

}