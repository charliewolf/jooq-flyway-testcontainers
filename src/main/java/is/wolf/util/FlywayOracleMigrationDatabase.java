package is.wolf.util;


import org.jooq.DSLContext;
import org.jooq.meta.oracle.OracleDatabase;
import org.testcontainers.containers.OracleContainer;

public class FlywayOracleMigrationDatabase extends OracleDatabase {


    private TestContainersConnectionManager testContainersConnectionManager = new TestContainersConnectionManager(OracleContainer.class);

    @Override
    protected DSLContext create0() {
        if(getConnection() == null) {
            setConnection(testContainersConnectionManager.getInternalConnection(getProperties()));
        }
        return super.create0();
    }

    @Override
    public void close() {
        testContainersConnectionManager.closeConnection();
        super.close();
    }

}