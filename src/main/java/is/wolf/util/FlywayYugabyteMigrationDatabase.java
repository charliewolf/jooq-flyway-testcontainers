package is.wolf.util;

import org.jooq.DSLContext;
import org.jooq.meta.yugabytedb.YugabyteDBDatabase;
import org.testcontainers.containers.YugabyteDBYSQLContainer;


public class FlywayYugabyteMigrationDatabase extends YugabyteDBDatabase {


    private TestContainersConnectionManager testContainersConnectionManager = new TestContainersConnectionManager(YugabyteDBYSQLContainer.class);

    @Override
    protected DSLContext create0() {
        if(getConnection() == null) {
            setConnection(testContainersConnectionManager.getInternalConnection(getProperties()));
        }
        return super.create0();
    }

    @Override
    public void close() {
        testContainersConnectionManager.closeConnection();
        super.close();
    }

}